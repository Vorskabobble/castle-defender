#include "Terrain.h"

Terrain::Terrain(){
	terrainData = NULL;
	normalsData = NULL;
}

Terrain::~Terrain(){
	if (terrainData){
		delete[] terrainData;
	}
	if (normalsData){
		delete[] normalsData;
	}
}

void Terrain::Initialise(){
	int scale = MAP_SCALE * MAP_RESOLUTION;
	terrainData = new float[scale * scale];
	normalsData = new Vector[scale * scale];
	LoadTextures* lt = new LoadTextures(false);
	lt->LoadBMP("Images/TerrainHeight.bmp");

	unsigned char *imageData = new unsigned char[scale * scale];

	for (int i = 0; i < scale * scale * 3; i += 3){
		imageData[i / 3] = lt->textureImageData[i];
	}

	for (int z = 0; z < scale; z++){
		for (int x = 0; x < scale; x++){
			terrainData[z * scale + x] = (float)imageData[z * scale + x] / 8;
			normalsData[z * scale + x] = calcNormal(x, z);
		}
	}

	lt->LoadGLTextures("Images/TerrainTex.bmp");
	textureID = lt->getTexture();
	delete lt;
	delete[] imageData;
}

void Terrain::Render(){
	float xp, zp;
	int scale = MAP_SCALE * MAP_RESOLUTION;

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	for (int x = 0; x < scale - 1; x++){
		glBindTexture(GL_TEXTURE_2D, textureID);
		glPushMatrix();
		glTranslatef(position.x, position.y, position.z);
		glBegin(GL_TRIANGLE_STRIP);
		for (int z = 0; z < scale - 1; z++){

			Vector norm;

			/* for each vertex, calc the greyscale shade colour & draw the vertex.
			the vertices are drawn in this order:
			2 -> 3
			<
			0 -> 1  */

			float fX = x;
			float fZ = z;

			float positionX = (float)x / MAP_RESOLUTION;
			float positionZ = (float)z / MAP_RESOLUTION;

			// draw vertex 0
			glNormal3f(norm.x, norm.y, norm.z);
			glTexCoord2f( fX / scale, fZ / scale );
			norm = normalsData[x + z * scale];
			glVertex3f(positionX, (terrainData[x + z * scale]), positionZ);

			// draw vertex 1
			glNormal3f(norm.x, norm.y, norm.z);
			glTexCoord2f((fX + 1.0f) / scale, fZ / scale);
			norm = normalsData[x + 1 + z * scale];
			glVertex3f(positionX + 1.0f, (terrainData[(x + 1) + z * scale]), positionZ);

			// draw vertex 2
			glNormal3f(norm.x, norm.y, norm.z);
			glTexCoord2f(fX / scale, (fZ + 1.0f) / scale);
			norm = normalsData[x + (z + 1) * scale];
			glVertex3f(positionX, terrainData[x + (z + 1) * scale], positionZ + 1.0f);

			// draw vertex 3
			glNormal3f(norm.x, norm.y, norm.z);
			glTexCoord2f((fX + 1.0f) / scale, (fZ + 1.0f) / scale );
			norm = normalsData[(x + 1) + (z + 1) * scale];
			glVertex3f(positionX + 1.0f, terrainData[x + 1 + (z + 1) * scale], positionZ + 1.0f);
		}
		glEnd();
		glPopMatrix();
	}
	glEnable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
}

float Terrain::getHeight(float x, float z){
	int tx = (x - position.x) * MAP_RESOLUTION;
	int tz = (z - position.z) * MAP_RESOLUTION;
	
	tx = tx < 0 ? 0 : tx;
	tz = tz < 0 ? 0 : tz;

	tx = tx > MAP_SCALE * MAP_RESOLUTION ? MAP_SCALE * MAP_RESOLUTION : tx;
	tz = tz > MAP_SCALE * MAP_RESOLUTION ? MAP_SCALE * MAP_RESOLUTION : tz;

	return (terrainData[tx + tz * MAP_SCALE * MAP_RESOLUTION]) + position.y;
}

void Terrain::setPosition(Vector pos){
	position = pos;
}

void Terrain::setArea( float x, float z, int width, int height, float terrainHeight, int smoothness )
{
	int tx = (x - position.x) * MAP_RESOLUTION;
	int tz = (z - position.z) * MAP_RESOLUTION;

	tx = tx < 0 ? 0 : tx;
	tz = tz < 0 ? 0 : tz;

	width = width * MAP_RESOLUTION;
	height = height * MAP_RESOLUTION;

	int scale = MAP_SCALE * MAP_RESOLUTION;

	width = tx + width > scale ? scale - tx : width;
	height = tz + height > scale ? scale - tz : height;

	for( int i = 0; i < width; ++i )
	{
		for( int y = 0; y < height; ++y )
		{
			terrainData[(tx + i) + ((tz + y) * scale)] = terrainHeight - position.y;
			normalsData[(tx + i) + ((tz + y) * scale)] = calcNormal( tx + i, tz + y );
		}
	}

	setBorderHeight( tx -1, tz -1, width +2, height +2, terrainHeight, smoothness );
}

void Terrain::setBorderHeight( int x, int z, int width, int height, float targetHeight, int levels )
{
	float currentHeight = terrainData[x + (z * MAP_SCALE * MAP_RESOLUTION)];
	x = x < 0 ? 0 : x;
	z = z < 0 ? 0 : z;

	width = x + width > MAP_SCALE * MAP_RESOLUTION ? MAP_SCALE * MAP_RESOLUTION - x : width;
	height = z + height > MAP_SCALE * MAP_RESOLUTION ? MAP_SCALE * MAP_RESOLUTION - z : height;

	for( int i = 0; i < width; ++i )
	{
		for( int y = 0; y < height; ++y )
		{
			if( i != 0 && y != 0 && i != width - 1 && y != height - 1 ) continue;
			float currentHeight = terrainData[(i + x) + ((z + y) * MAP_SCALE * MAP_RESOLUTION)];
			float modifier = (targetHeight - currentHeight) - ((targetHeight - currentHeight) / levels);
			terrainData[(i + x) + ((z + y) * MAP_SCALE * MAP_RESOLUTION)] = currentHeight + modifier;
		}
	}

	if( levels > 1 )
	{
		setBorderHeight( x - 1, z - 1, width + 2, height + 2, targetHeight, levels - 1 );
	}
}

Vector Terrain::calcNormal(int x, int z){
	int scale = MAP_SCALE * MAP_RESOLUTION;
	Vector center, up, down, left, right;
	Vector centerNormal, upNormal, downNormal, leftNormal, rightNormal;

	center = Vector( x, terrainData[x + (z * scale)], z);
	up = down = left = right = center;

	++up.z;
	up.y = z < scale ? terrainData[x + ((z + 1) * scale)] : up.y;

	--down.z;
	down.y = z > 0 ? terrainData[x + ((z - 1) * scale)] : down.y;

	--left.x;
	left.y = x > 0 ? terrainData[(x - 1) + (z * scale)] : left.y;

	++right.z;
	right.y = z < scale ? terrainData[(x + 1) + (z * scale)] : right.y;

	upNormal = NormToPlane( up, right, center );
	downNormal = NormToPlane( down, left, center );
	leftNormal = NormToPlane( left, up, center );
	rightNormal = NormToPlane( right, down, center );

	centerNormal = upNormal + downNormal + leftNormal + rightNormal;
	centerNormal.Normalize();
	//return -centerNormal;
	return Vector( 0, 1, 0 );

	//int scale = MAP_SCALE * MAP_RESOLUTION;
	//Vector p0, p1, p2, p3, p4, n1, n2, n3, n4, sn;
	//p0.x = x; p0.y = terrainData[x + z * scale]; p0.z = -z;
	//p1 = p2 = p3 = p4 = p0;
	//if (z > 0){
	//	p1.y = terrainData[x + (z - 1) * scale]; //p1 below p0
	//	p1.z = -(z - 1);
	//}
	//if (x > 0){
	//	p2.x = (x - 1); //p2 left of p0
	//	p2.y = terrainData[x - 1 + z * scale];
	//}
	//if (z < scale){
	//	p3.y = terrainData[x + (z + 1) * scale]; //p3 above p0
	//	p3.z = -(z + 1);
	//}
	//if (x < scale){
	//	p4.x = (x + 1); //p4 right of p0
	//	p4.y = terrainData[x + 1 + z * scale];
	//}

	//n1 = NormToPlane(p0, p2, p1); //calc normals for each pt around p0
	//n2 = NormToPlane(p0, p3, p2);
	//n3 = NormToPlane(p0, p4, p3);
	//n4 = NormToPlane(p0, p1, p4);

	//sn = -n1 + -n2 + -n3 + -n4;  // sum normals
	//sn.Normalize();
	//return sn;
}
