#pragma once
#include "LoadTextures.h"
#include "vector.h"
#include "MathFunctions.h"
#include "Object.h"

const int MAP_SCALE = 256;
const int MAP_RESOLUTION = 1;

class Terrain{
private:
	float* terrainData;
	Vector* normalsData;
	int textureID;

	Vector position;
public:
	Terrain();
	~Terrain();

	void Initialise();
	void Render();

	Vector calcNormal(int x, int z);

	float getHeight(float x, float z);
	void setPosition(Vector pos);

	void setArea( float x, float y, int width, int height, float terrainHeight, int smoothness );
private:
	void setBorderHeight( int x, int z, int width, int height,float targetHeight, int levels );
};

