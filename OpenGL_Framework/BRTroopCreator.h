#pragma once
#include "PlayerTroop.h"
#include "ModelLoader.h"
#include "CollisionEngine.h"

#include "BRMilitia.h"

enum BRTROOPTYPE{
	BRMILITIA = 0,
	BRSWORDSMAN,
	BRARCHER
};

class BRTroopCreator{
private:
	ModelLoader* loader;
	CollisionEngine* colEng;
public:
	BRTroopCreator();
	~BRTroopCreator();

	PlayerTroop* CreateTroop(BRTROOPTYPE type);
};

