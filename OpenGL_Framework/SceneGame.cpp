#include "SceneGame.h"

SceneGame::SceneGame(){
	castle = new SmallCastle();
	terrain = NULL;
}

SceneGame::~SceneGame(){
	if (castle){
		delete castle;
	}
	if (tGen){
		delete tGen;
	}
	if (spawner){
		delete spawner;
	}
	if (terrain){
		delete terrain;
	}

	while (!barbarians.empty()){
		delete barbarians[barbarians.size() - 1];
		barbarians.pop_back();
	}
	collisionEngine->DeleteTree();
	GUIHandle->ReleaseWindow("game");
	sndEngine->drop();
}

void SceneGame::Initialise(){
	collisionEngine->CreateTree(0, 0, 0, 50, 50, 50);

	guitimer = 0.0f;

	//randomly create the terrain
	tGen = new TerrainGenerator();
	tGen->generateMap(LAND, MAP_SCALE * MAP_RESOLUTION);
	terrain = new Terrain();
	terrain->Initialise();
	terrain->setPosition(Vector(-128, 0, -128));
	castle->Initialise(terrain->getHeight(0, 0), terrain);

	terrain->setArea( -15, -10, 70, 20, terrain->getHeight( 0, 0 ), 15 );

	mainCamera->getMove()->getPosition().set(10, terrain->getHeight(10, 40) + 20, 40);
	mainCamera->getMove()->rotateTo(X, 30);

	numEnemies = 1;

	spawner = new BRTroopCreator();
	spawnTimer = 0.0f;
	spawnSpeed = 10.0f;

	gameTime = 0.0f;

	director->setGameInfo("gold", 300);

	//Creating GUI elements for the scene
	GUIHandle->newWindow("game", 0, 0, 100, 100);
	GUIHandle->newElement(BUTTON, "gold", Game->SCREEN.width / 2 - 150, Game->SCREEN.height - 210, 300, 75, "game");
	GUIHandle->getWindow("game")->getElement("gold")->setFont(50);
	GUIHandle->newElement(BUTTON, "time", Game->SCREEN.width / 2 - 100, 20, 200, 75, "game");
	GUIHandle->getWindow("game")->getElement("time")->setFont(50);

	GUIHandle->newElement(BUTTON, "goldlvl", Game->SCREEN.width / 2 + 10, Game->SCREEN.height - 100, 200, 75, "game");
	GUIHandle->getWindow("game")->getElement("goldlvl")->setRender(false);
	GUIHandle->getWindow("game")->getElement("goldlvl")->setFont(50);
	GUIHandle->getWindow("game")->getElement("goldlvl")->setColour(Colour{ 0.0f, 0.8f, 0.0f, 1.0f });

	sndEngine = createIrrKlangDevice();
//	sndEngine->setSoundVolume(0.3f);
	sndEngine->play2D("Sounds/Prelude and Action.mp3", true);
}

void SceneGame::Update(){
	//Update game info with camera position
	Game->SCREEN.camX = mainCamera->getMove()->getPosition().x;
	Game->SCREEN.camY = mainCamera->getMove()->getPosition().y;
	Game->SCREEN.camZ = mainCamera->getMove()->getPosition().z;

	collisionEngine->Update();

	spawnTimer += Game->TIME.delta;
	gameTime += Game->TIME.delta;

	//spawn enemies at a set interval that decreases over time
	if (spawnTimer > spawnSpeed){
		numEnemies++;
		PlayerTroop* t_troop = spawner->CreateTroop(BRMILITIA);
		t_troop->getMove()->getPosition().set(-100, 0, 0);
		barbarians.push_back(t_troop);
		spawnTimer = 0;
		spawnSpeed -= Game->TIME.delta * 3;
		if (spawnSpeed < 1.0f){
			spawnSpeed = 1.0f;
		}
		int upgrade = numEnemies / 10;
		for (int i = 0; i < upgrade; i++){
			t_troop->Upgrade(1.3f);
		}
	}

	//Update castle and pass gold variable for use
	castle->setGold(director->getGameInfo("gold"));
	castle->Update();
	if (castle->Defeated()){
		director->unloadScene("Game");
		director->changeScene("Menu");
	}

	//set updated gold variable
	director->setGameInfo("gold", castle->getGold());

	//update all of the barbarians
	for (auto& troop : barbarians){
		troop->Update();
		troop->getMove()->move(X, troop->getStats()->getSpeed());
		troop->getMove()->getPosition().y = terrain->getHeight( troop->getMove()->getPosition().x, troop->getMove()->getPosition().z );
	}

	//delete a barbarian if their health is 0
	int bSize = barbarians.size();
	barbarians.erase(std::remove_if(barbarians.begin(), barbarians.end(), [](PlayerTroop* troop)
	{
		if (troop->getStats()->getHealth() <= 0){
			delete troop;
			return true;
		}
		return false;
	}), barbarians.end());

	//update players gold based on the amount of barbarians killed
	if (bSize > barbarians.size()){
		director->setGameInfo("gold", director->getGameInfo("gold") + (bSize - barbarians.size()) * (50 * (1 + numEnemies/10)));
		char s[100];
		float t = (bSize - barbarians.size()) * (50 * (1 + numEnemies / 10));
		sprintf_s(s, "+%1.0fG", t);
		GUIHandle->getWindow("game")->getElement("goldlvl")->setText(s);
		guitimer = 3.0f;
	}

	//increase gold over time
	director->setGameInfo("gold", director->getGameInfo("gold") + Game->TIME.delta * 2);

	//show gold difference for a short amount of time
	if (guitimer > 0){
		GUIHandle->getWindow("game")->getElement("goldlvl")->setRender(true);
		guitimer -= Game->TIME.delta;
	}
	else{
		guitimer = 0;
		GUIHandle->getWindow("game")->getElement("goldlvl")->setRender(false);
	}

	//show total gold amount
	char c[100];
	sprintf_s(c, "Gold : %1.0f", director->getGameInfo("gold"));
	//sprintf_s( c, "Camera (%f, %f, %f)", mainCamera->getMove()->getPosition().x, mainCamera->getMove()->getPosition().y, mainCamera->getMove()->getPosition().z );
	GUIHandle->getWindow("game")->getElement("gold")->setText(c);

	sprintf_s(c, "%1.0f", gameTime);
	//sprintf_s( c, "terrain height (%f)", terrain->getHeight( mainCamera->getMove()->getPosition().x, mainCamera->getMove()->getPosition().z ));
	GUIHandle->getWindow("game")->getElement("time")->setText(c);


	//camera input
	if (Game->INPUT.keyPressed[VK_W]){
		mainCamera->getMove()->localMove(Z, -10.0f);
	}
	if (Game->INPUT.keyPressed[VK_S]){
		mainCamera->getMove()->localMove(Z, 10.0f);
	}
	if (Game->INPUT.keyPressed[VK_A]){
		mainCamera->getMove()->localMove(X, 10.0f);
	}
	if (Game->INPUT.keyPressed[VK_D]){
		mainCamera->getMove()->localMove(X, -10.0f);
	}
	if (Game->INPUT.keyPressed[VK_Q]){
		mainCamera->getMove()->localMove(Y, -10.0f);
	}
	if (Game->INPUT.keyPressed[VK_E]){
		mainCamera->getMove()->localMove(Y, 10.0f);
	}
	if (Game->INPUT.keyPressed[VK_RBUTTON]){
		if (Game->INPUT.lmouseX > 0){
			mainCamera->getMove()->rotateBy(Y, (Game->INPUT.lmouseX - Game->INPUT.mouseX) * 5);
		}
		if (Game->INPUT.lmouseY > 0 && mainCamera->getMove()->getRotation().x < 89 && mainCamera->getMove()->getRotation().x > -89){
			mainCamera->getMove()->rotateBy(X, (Game->INPUT.mouseY - Game->INPUT.lmouseY) * 5);
		}
		else if( mainCamera->getMove()->getRotation().x > 89 )
		{
			mainCamera->getMove()->getRotation().x = 87;
		}
		else if( mainCamera->getMove()->getRotation().x < -89 )
		{
			mainCamera->getMove()->getRotation().x = -87;
		}

	}
	//end of camera input

	if( Game->INPUT.keyPressed[VK_O] )
	{
		mainCamera->getMove()->getPosition().set( 0, 0, 0 );
	}

	if( Game->INPUT.keyPressed[VK_C] )
	{
		mainCamera->getMove()->getPosition().set( 0, terrain->getHeight( 0, 0 ), 0 );
	}

	if( mainCamera->getMove()->getPosition().y - 5.0f < terrain->getHeight( mainCamera->getMove()->getPosition().x, mainCamera->getMove()->getPosition().z ) )
	{
		mainCamera->getMove()->getPosition().y = terrain->getHeight( mainCamera->getMove()->getPosition().x, mainCamera->getMove()->getPosition().z ) + 5.0f;
	}
}

void SceneGame::Render(){
	mainCamera->Update();

	terrain->Render();
	castle->Render();
	collisionEngine->Render();

	for (auto& troop : barbarians){
		troop->Render();
	}
}
