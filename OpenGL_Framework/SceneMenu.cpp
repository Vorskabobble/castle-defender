#include "SceneMenu.h"


SceneMenu::SceneMenu(){
	cube = NULL;
	game = NULL;
}

SceneMenu::~SceneMenu(){
	if (cube){
		delete cube;
	}
	if (game){
		director->unloadScene("Game");
		delete game;
	}
}

void SceneMenu::Initialise(){
	modelLoader->LoadModel("cube", "Models/TestPlayer.obj");
	cube = new Object("cube");
	cube->setModel(modelLoader->getModel("cube"));
	//cube->getModel()->loadTexture("Images/sky.bmp");
	cube->getMove()->getPosition().set( 3.0f, -2.0f, -6.0f );

	//set GUI for the scene menu
	GUIHandle->newWindow("menu", 0, 0, Game->SCREEN.width, Game->SCREEN.height);
	GUIHandle->newElement(BUTTON, "start", Game->SCREEN.width / 2 - 100, Game->SCREEN.height/2 - 37.5, 200, 75, "menu");
	GUIHandle->getWindow("menu")->getElement("start")->setFont(50);
	GUIHandle->getWindow("menu")->getElement("start")->setText("Start");
	GUIHandle->getWindow( "menu" )->getElement( "start" )->setColour( COLOUR_ORANGE );

	GUIHandle->newElement(BUTTON, "htp1", 10, 100, 900, 50, "menu");
	GUIHandle->getWindow("menu")->getElement("htp1")->setFont(30);
	GUIHandle->getWindow("menu")->getElement("htp1")->setText("Move the Camera with 'W' 'A' 'S' 'D'");
	GUIHandle->newElement(BUTTON, "htp2", 10, 160, 900, 50, "menu");
	GUIHandle->getWindow("menu")->getElement("htp2")->setFont(30);
	GUIHandle->getWindow("menu")->getElement("htp2")->setText("Move up and down with 'Q' and 'E'");
	GUIHandle->newElement(BUTTON, "htp3", 10, 220, 900, 50, "menu");
	GUIHandle->getWindow("menu")->getElement("htp3")->setFont(30);
	GUIHandle->getWindow("menu")->getElement("htp3")->setText("Click the barracks(green box) to spawn troops!");
	GUIHandle->newElement(BUTTON, "htp4", 10, 280, 900, 50, "menu");
	GUIHandle->getWindow("menu")->getElement("htp4")->setFont(30);
	GUIHandle->getWindow("menu")->getElement("htp4")->setText("Dont let the barbarians get to the throne!");
}

void SceneMenu::Update(){
	if (game){
		delete game;
		game = NULL;
	}
	GUIHandle->getWindow("menu")->setActive(true);
	GUIHandle->getWindow("menu")->setRender(true);
	cube->getMove()->rotateBy(Y, 20.0f);

	//change scene if button is clicked
	if (GUIHandle->getWindow("menu")->getElement("start")->getState()->used){
		game = new SceneGame();
		director->setGameInfo("Menu", 0.0f);
		director->loadScene("Game", *game);
		director->changeScene("Game");
		GUIHandle->getWindow("menu")->setActive(false);
		GUIHandle->getWindow("menu")->setRender(false);
		GUIHandle->getWindow("menu")->getElement("start")->getState()->used = false;
	}
}

void SceneMenu::Render(){
	mainCamera->Update();
	cube->Render();
}