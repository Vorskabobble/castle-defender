#pragma once
#include "Castle.h"
class SmallCastle : public Castle{
public:
	SmallCastle();
	~SmallCastle();

	void Initialise(float height, Terrain* terrain);
};

